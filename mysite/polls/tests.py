from django.test import TestCase


# Create your tests here.

import datetime
from .models import Choice, Question

from django.utils import timezone
  
class QuestionTestCase(TestCase):
    def setUp(self):
        q = Question(question_text="how is it?", pub_date=timezone.now())
        q.save()
  
    def test_Questions_in_current_year(self):
        """Questions is in current year"""
        q = Question.objects.get(pk=1)
        self.assertTrue(q.was_published_recently())

    def test_Questions_willfail(self):
        """this test will fail! on purpose!"""
        q = Question.objects.get(pk=1)
        self.assertTrue(q.question_text.startswith("what"))
