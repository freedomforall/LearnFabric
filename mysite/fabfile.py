from __future__ import with_statement
from fabric.api import local, settings, abort, run, cd
from fabric.contrib.console import confirm


#def test():
#	local("python ./manage.py test polls")

def test():
    with settings(warn_only=True):
        result = local('python ./manage.py test polls', capture=True)
    if result.failed and not confirm("Tests failed. Continue anyway?"):
        abort("Aborting at user request.")

def deploy():
    code_dir = r'G:\LOCALSVN\PYTHON\TestFabricDjango'
    with cd(code_dir):
        run("git pull")
        run("touch app.wsgi")

def commit():
	local('git add -A && git commit  -m "a,c,p by fab -- functions"')

def push():
	local("git push")
	
def prepare_deploy():
	test()
	commit()
	push()  
	  
    